const rewireSass = require('react-app-rewire-sass');
const rewireMobx = require('react-app-rewire-mobx');


module.exports = function override(config, env){

    rewireMobx(config, env);
    rewireSass(config, env);

    return config;
}