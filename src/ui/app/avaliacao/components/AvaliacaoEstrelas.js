import React from 'react';
import StarRatingComponent from 'react-star-rating-component';
import { amber400 } from 'material-ui/styles/colors';

class AvaliacaoEstrelas extends React.Component {

 
    renderStar(index, v, name) {
        var pos = index
        var value = this.props.value;

        if (value % 1 != 0) {
            if (Math.ceil(value) == pos) {
                return <i className="mdi mdi-star-half" />;
            }
        }

        if (pos <= value) {
            return <i className="mdi mdi-star" />;
        }

        return <i className="mdi mdi-star-outline" />;

    }

    handleStarSelect(ev, rating) {

        if (this.props.value == rating - 0.5) {
            this.props.onChange(ev, rating);
        } else if (this.props.value == rating) {
            this.props.onChange(ev, rating - 0.5);
        } else {
            this.props.onChange(ev, rating);
        }

    }

    render() {

        const { value } = this.props;

        var r = Math.floor(value * 2) / 2;

        return (
            <div className="avaliacao-component">

                <p className="avaliacao-value" style={{ color: amber400 }}>{r.toFixed(1).replace(".", ",")}</p>

                <StarRatingComponent
                    name="rate1"
                    starCount={5}
                    value={value}
                    renderStarIconHalf={(i, v, n) => this.renderStar(i, v, n)}
                    renderStarIcon={(i, v, n) => this.renderStar(i, v, n)}
                    starColor={amber400}
                    emptyStarColor={amber400}
                    onStarClick={(rating, ev) => this.handleStarSelect(ev, rating)}
                />
                <p className="hint">(Toque novamente na estrela para metade.)</p>
            </div>
        );

    }


}


export default AvaliacaoEstrelas;