import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';

@withRouter
@inject("avaliacao")
@observer
class DialogLoading extends React.Component {

    dismiss = () => {

    }

    render() {




        return (
            <Dialog

                modal
                open={this.props.avaliacao.loading}
                onRequestClose={this.dismiss} >
                <div style={{ float: "left", marginRight: "8px" }}>
                    <CircularProgress size={50} thickness={4} />
                </div>
                <h3 style={{ paddingLeft: "16px", float: "left" }}> Enviando... </h3>
            </Dialog>
        );

    }


}


export default DialogLoading;