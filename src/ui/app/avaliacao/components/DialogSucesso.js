import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';

@withRouter
@inject("avaliacao")
@observer
class DialogSucesso extends React.Component {

    dismiss = () => {
        this.props.router.push(`/${this.props.params.empresaId}`)
        this.props.avaliacao.limpar();
    }

    render() {

        const actions = [
            <FlatButton
                label="OK!"
                primary
                onTouchTap={this.dismiss}
            />,

        ];


        return (
            <Dialog
                actions={actions}
                modal
                open={this.props.avaliacao.sucesso}
                onRequestClose={this.dismiss} >
                {this.props.avaliacao.sucesso}
            </Dialog>
        );

    }


}


export default DialogSucesso;