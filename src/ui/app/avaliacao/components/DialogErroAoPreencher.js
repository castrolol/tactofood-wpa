import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { observer, inject } from 'mobx-react';

@inject("avaliacao")
@observer
class DialogErroAoPreencher extends React.Component {

    dismiss = () => {
        setTimeout(() => this.props.avaliacao.error = "", 200);
    }

    render() {

        const actions = [
            <FlatButton
                label="OK!"
                primary
                onTouchTap={this.dismiss}
            />,

        ];


        return (
            <Dialog
                actions={actions}
                modal
                open={this.props.avaliacao.error}
                onRequestClose={this.dismiss} >
                {this.props.avaliacao.error}
            </Dialog>
        );

    }


}


export default DialogErroAoPreencher;