import React from 'react';
import StarRatingComponent from 'react-star-rating-component';
import TextField from 'material-ui/TextField';
import AvaliacaoEstrelas from './components/AvaliacaoEstrelas';
import DialogErroAoPreencher from './components/DialogErroAoPreencher';
import DialogSucesso from './components/DialogSucesso';
import DialogLoading from './components/DialogLoading';
import { amber400 } from 'material-ui/styles/colors';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import './avaliacao-page.scss';

@withRouter
@inject("app", "cardapio","avaliacao")
@observer
class AvaliacaoPage extends React.Component {



    componentWillMount() {
        var cardapio = this.props.cardapio;

        this.props.app.setPage((cardapio.empresa && cardapio.empresa.nomeFantasia) || "", "mdi mdi-36-bar mdi-36px mdi-check", () => {
            this.props.avaliacao.enviar();
        });

        this.props.avaliacao && this.props.avaliacao.limpar();
    }

    componentWillReact() {
        var cardapio = this.props.cardapio;
        
        this.props.app.setPage((cardapio.empresa && cardapio.empresa.nomeFantasia) || "", "mdi mdi-36-bar mdi-36px mdi-check", () => {
            this.props.avaliacao.enviar();
        });

    }

    link = (prop) => {

        if (!this.props.avaliacao) return {};

        var avaliacao = this.props.avaliacao;

        return {
            value: avaliacao[prop],
            onChange: (e, v) => {

                avaliacao[prop] = v
            },
            errorText: avaliacao[prop + "Error"]
        }

    }

    render() {




        return (
            <div className="avaliacao-page">
                <DialogErroAoPreencher />
                <DialogSucesso />
                <DialogLoading />
                <h1 className="avaliacao-title">Avalie nosso estabelecimento!</h1>
                <AvaliacaoEstrelas {...this.link("estrelas") } />

                <div className="avaliacao-form">

                    <TextField
                        {...this.link("comentario") }
                        fullWidth
                        floatingLabelText="Comentário"
                        multiLine={true}
                        rows={3}
                        rowsMax={5} />

                    <TextField
                        {...this.link("nome") }
                        fullWidth
                        floatingLabelText="Nome Completo" />

                    <TextField
                        {...this.link("email") }
                        fullWidth
                        floatingLabelText="Email" />

                    <TextField
                        {...this.link("telefone") }
                        fullWidth
                        floatingLabelText="Telefone" />
                </div>
            </div>
        );

    }


}


export default AvaliacaoPage;