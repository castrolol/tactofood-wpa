import React from 'react';
import 'react-responsive-carousel/lib/styles/carousel.css';

import CategoriaLista from './components/CategoriaLista';
import PromocoesCarousel from './components/PromocoesCarousel';
import RestauranteHeader from './components/RestauranteHeader';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';

@withRouter
@inject("app")
@observer
class CategoriasPage extends React.Component {


    componentWillMount() {

        this.props.app.setPage("Cardápio");
    }

    render() {

        return (
            <div>
                <RestauranteHeader />
                <PromocoesCarousel />
                <CategoriaLista />
            </div>
        );

    }


}


export default CategoriasPage;