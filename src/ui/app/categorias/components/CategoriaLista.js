import React from 'react';
import { withRouter } from 'react-router';
import CategoriaItem from './CategoriaItem';
import { List } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import { observer, inject } from 'mobx-react';

const subheaderStyle = {
    color: "#ec6464",
    height: "30px",
    lineHeight: "40px",
    marginTop: "8px"
};

@withRouter
@inject("cardapio")
@observer
class CategoriaLista extends React.Component {



    navigateTo(grupo) {
        var empresaId = this.props.params.empresaId;
        this.props.router.push(`/${empresaId}/grupo/${grupo.id}`);
    }


    render() {

        const { cardapio } = this.props;
        const { loading, categorias } = cardapio;


        return (

            <List>

                {categorias.map((categoria, i) =>
                    <div key={categoria.id}>
                        {i ? <Divider /> : null}
                        <Subheader style={subheaderStyle} > {categoria.nome}</Subheader>
                        {categoria.grupos.map(grupo =>
                            <CategoriaItem key={grupo.id} title={grupo.nome} onClick={() => this.navigateTo(grupo)} />
                        )}
                    </div>
                )}

            </List>
        );

    }
}


export default CategoriaLista;