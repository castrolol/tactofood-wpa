import React from 'react';
import { Carousel } from 'react-responsive-carousel';

import Divider from 'material-ui/Divider';
import { observer, inject } from 'mobx-react';

@inject("cardapio")
@observer
class PromocoesCarousel extends React.Component {

    constructor(props) {
        super(props);
    }

    //má pratica, fiz para evitar um atraso no componente de carousel
    updateText(index) {
        return;
        const { cardapio } = this.props;
        const { promocoes } = cardapio;

        this.refs.text.textContent = promocoes[index].text;

    }

    render() {

        const { cardapio } = this.props;
        const { promocoes, empresa } = cardapio;
        const currentIndex = (this.refs.carousel && this.refs.carousel.selectedItem) || 0;

        if (!promocoes || !promocoes.length) return null;
       // if (empresa && !empresa.exibirPromocao) return null;

        return (
            <div>
                <Carousel ref="carousel" autoPlay infiniteLoop onChange={(index) => this.updateText(index)} emulateTouch showStatus={false} showThumbs={false} showArrows={true} >
                    {promocoes.map(promocao =>
                        <div key={promocao.id} >
                            <img src={promocao.image} />
                        </div>
                    )}
                </Carousel>

                {/*<p ref="text" style={{ color: "#777", height: "60px", margin: "8px" }}>
                    {promocoes[currentIndex] && promocoes[currentIndex].text || ""}
                </p>*/}
                <Divider />
            </div>
        );

    }
}


export default PromocoesCarousel;