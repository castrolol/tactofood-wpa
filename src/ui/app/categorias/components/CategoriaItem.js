import React from 'react';
import { ListItem } from 'material-ui/List';

const CategoriaItem = (props) => {


    return (
        <ListItem
            onTouchTap={props.onClick}
            primaryText={props.title} />
    )

};

export default CategoriaItem;