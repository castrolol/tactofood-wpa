import React from 'react';
import { browserHistory } from 'react-router';
import { CSSTransitionGroup } from 'react-transition-group' // ES6
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import Layout from '../common/Layout';
import SplashScreen from '../common/SplashScreen';

@inject("nav", "cardapio")
@observer
class App extends React.Component {

    componentWillMount() {
        console.log(this.props);
        this.props.cardapio.load(this.props.params.empresaId);

    }

    componenWillUpdate(nextProps) {
        if (this.props.params != nextProps.params) {
            if (this.props.params.empresaId != nextProps.params.empresaId) {
                this.props.cardapio.load(nextProps.params.empresaId);
            }
        }
    }

    render() {

        var transitionName = "fade-in-up";



        return (
            <Layout>

                <CSSTransitionGroup
                    transitionName={transitionName}
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}>

                    {React.cloneElement(this.props.children, { key: window.location.pathname })}
                </CSSTransitionGroup>
                <SplashScreen />
            </Layout>

        );

    }


}


export default App;