import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import { red400, blue500 } from 'material-ui/styles/colors';

const reclamacaoColor = red400;
const sugestaoColor = blue500;

class FeedbackPicker extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            open: false
        };
    }

    choose(ev, tipo) {
        this.setState({
            open: false
        })
        this.props.onChange && this.props.onChange(ev, tipo);

    }


    openToChoose() {
        this.setState({
            open: true
        })
    }

    close() {
        this.setState({
            open: false
        });
    }

    render() {
        var value = this.props.value || "sugestao";
        var color = value == "reclamacao" ? reclamacaoColor : sugestaoColor;
        var texto = value == "reclamacao" ? "Reclamação" : "Sugestão";

        return (

            <div className="feedback-type">
                <Dialog open={this.state.open}
                    title="O que deseja fazer?"
                    onRequestClose={() => this.close()} >

                    <div className="feedback-button-container">
                        <RaisedButton
                            fullWidth
                            onTouchTap={(ev) => this.choose(ev, "sugestao")}
                            labelColor={"white"}
                            backgroundColor={sugestaoColor} label="uma Sugestão" />

                    </div>
                    <div className="feedback-button-container">

                        <RaisedButton
                            fullWidth
                            labelColor={"white"}
                            onTouchTap={(ev) => this.choose(ev, "reclamacao")}
                            backgroundColor={reclamacaoColor} label="uma Reclamação" />
                    </div>

                </Dialog>

                <h1> Eu gostaria de fazer uma... </h1>
                <RaisedButton onTouchTap={() => this.openToChoose()} fullWidth labelColor={"white"} backgroundColor={color} label={texto} />
            </div>
        );

    }

}

export default FeedbackPicker;