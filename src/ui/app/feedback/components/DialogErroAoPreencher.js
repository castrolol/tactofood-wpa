
import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { observer, inject } from 'mobx-react';

@inject("feedback")
@observer
class DialogErroAoPreencher extends React.Component {

    dismiss = () => {
        setTimeout(() => this.props.feedback.error = "", 200);
    }

    render() {

        const actions = [
            <FlatButton
                label="OK!"
                primary
                onTouchTap={this.dismiss}
            />,

        ];


        return (
            <Dialog
                actions={actions}
                modal
                open={this.props.feedback.error}
                onRequestClose={this.dismiss} >
                {this.props.feedback.error}
            </Dialog>
        );

    }


}


export default DialogErroAoPreencher;