import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';

@withRouter
@inject("feedback")
@observer
class DialogSucesso extends React.Component {

    dismiss = () => {
        this.props.router.push(`/${this.props.params.empresaId}`)
        this.props.feedback.limpar();
    }

    render() {

        const actions = [
            <FlatButton
                label="OK!"
                primary
                onTouchTap={this.dismiss}
            />,

        ];


        return (
            <Dialog
                actions={actions}
                modal
                open={this.props.feedback.sucesso}
                onRequestClose={this.dismiss} >
                {this.props.feedback.sucesso}
            </Dialog>
        );

    }


}


export default DialogSucesso;