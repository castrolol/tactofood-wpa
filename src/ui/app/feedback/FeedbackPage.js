import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { observer, inject } from 'mobx-react';
import FeedbackPicker from './components/FeedbackPicker';
import DialogErroAoPreencher from './components/DialogErroAoPreencher';
import DialogLoading from './components/DialogLoading';
import DialogSucesso from './components/DialogSucesso';
import './feedback-page.scss'

@inject("app", "feedback")
@observer
class FeedbackPage extends React.Component {


    componentWillMount() {
        this.props.app.setPage("Feedback", "mdi mdi-36-bar mdi-36px mdi-check", () => {
            this.props.feedback.enviar();
        });

        this.props.feedback && this.props.feedback.limpar();
    }


    link = (prop) => {

        if (!this.props.feedback) return {};

        var feedback = this.props.feedback;

        return {
            value: feedback[prop],
            onChange: (e, v) => {

                feedback[prop] = v
            },
            errorText: feedback[prop + "Error"]
        }

    }

    render() {


        const { feedback } = this.props;
        const { tipo } = feedback;

        const tipoDesc = tipo == "reclamacao" ? "reclamação" : "sugestão";

        return (
            <div className="feedback-page">
                <DialogErroAoPreencher />
                <DialogLoading />
                <DialogSucesso />
                <div className="feedback-form">

                    <FeedbackPicker {...this.link("tipo") } />

                    <TextField
                        {...this.link("descricao") }
                        fullWidth
                        floatingLabelText={`Descreva sua ${tipoDesc}`}
                        multiLine={true}
                        rows={3}
                        rowsMax={5} />

                    <TextField
                        {...this.link("nome") }
                        fullWidth
                        floatingLabelText="Nome Completo" />

                    <TextField
                        {...this.link("email") }
                        fullWidth
                        floatingLabelText="Email" />

                    <TextField
                        {...this.link("telefone") }
                        fullWidth
                        floatingLabelText="Telefone" />
                </div>
            </div>
        );

    }


}


export default FeedbackPage;