import React from 'react';
import ProdutosLista from './components/ProdutosLista';
import GruposLista from './components/GruposLista';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';

const subheaderStyle = {
    color: "#ec6464",
    height: "30px",
    lineHeight: "40px",
    marginTop: "8px"
};


@withRouter
@inject("app", "cardapio")
@observer
class GrupoPage extends React.Component {

    get grupo() {

        const { cardapio, params } = this.props;
        const { grupoId } = params;
        return cardapio.grupos.filter(grupo => grupo.id == grupoId)[0] || null;
    }

    componentWillMount() {
        if (this.grupo) {
            this.props.app.setPage(this.grupo.nome || " ");
        }
    }

    componentWillReact() {
        if (this.grupo) {
            this.props.app.setPage(this.grupo.nome || " ");

        }
    }

    render() {

        const { grupo } = this;

        if (!this.grupo) return <span />;


        return (
            <div>

                {grupo.produtos ?
                    <ProdutosLista produtos={grupo.produtos} /> :
                    <GruposLista grupos={grupo.filhos} />
                }

            </div>
        );

    }


}


export default GrupoPage;