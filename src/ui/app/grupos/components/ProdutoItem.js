import React from 'react';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';


class ProdutoItem extends React.Component {




    render() {

        const { produto } = this.props;

        return (

            <Card>

                <CardMedia >
                    <img src={produto.urlImagemMobile || produto.urlImagem} alt={produto.nome} />
                </CardMedia>
                <CardTitle title={produto.nome} />
                <CardText>{produto.texto} </CardText>

            </Card>
        );

    }
}


export default ProdutoItem;