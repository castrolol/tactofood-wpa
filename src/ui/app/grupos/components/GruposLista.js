import React from 'react';
import { withRouter } from 'react-router';
import CategoriaItem from '../../categorias/components/CategoriaItem';
import { List, ListItem } from 'material-ui/List';


class GruposLista extends React.Component {




    render() {

        const { grupos } = this.props;

        return (

            <List>

                {grupos.map(grupo =>
                    <ListItem key={grupo.id} primaryText={grupo.nome} />
                )}
            </List>
        );

    }
}


export default GruposLista;