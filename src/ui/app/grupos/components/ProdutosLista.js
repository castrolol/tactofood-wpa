import React from 'react';
import { List, ListItem } from 'material-ui/List';
import ProdutoItem from './ProdutoItem';

class ProdutosLista extends React.Component {




    render() {

        const { produtos } = this.props;

        return (

            <div style={{ padding: "16px 8px" }}>

                {produtos.map(produto =>
                    <ProdutoItem key={produto.id} produto={produto} />
                )}
            </div>
        );

    }
}


export default ProdutosLista;