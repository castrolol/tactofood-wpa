import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import FontIcon from 'material-ui/FontIcon';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import TactoFoodPanel from './TactoFoodPanel';

const subheaderStyle = {
    color: "#ec6464",
};
const iconStyle = {
    color: "#444",
    position: "relative",
    top: "3px",
    marginRight: "3px"
};

@withRouter
@inject("menu")
@observer
class Sidebar extends React.Component {

    navigateTo(path) {
        this.props.router.push(`/${this.props.params.empresaId}/${path}`);
        this.props.menu.toggle();
    }

    render() {
        const { menu } = this.props;

        return (
            <Drawer
                docked={false}
                open={menu.open}
                onRequestChange={(open) => menu.toggle()} >
                <TactoFoodPanel />
                <MenuItem onTouchTap={() => this.navigateTo("")} > <FontIcon style={iconStyle} className="mdi mdi-book-open-variant" /> Cardápio </MenuItem>
                <MenuItem onTouchTap={() => this.navigateTo("feedback")} > <FontIcon style={iconStyle} className="mdi mdi-email-open-outline" /> Sugestões &amp; Reclamações</MenuItem>
                <MenuItem onTouchTap={() => this.navigateTo("avaliacao")}> <FontIcon style={iconStyle} className="mdi mdi-star-outline" /> Avalie !</MenuItem>
                <Divider />
                <Subheader style={subheaderStyle}>
                    Fotos
                </Subheader>
                <MenuItem> <FontIcon style={iconStyle} className="mdi mdi-image-filter" /> Fotos de Cuiabá</MenuItem>
                <MenuItem> <FontIcon style={iconStyle} className="mdi mdi-image-filter" /> Aves de Mato Grosso</MenuItem>

            </Drawer>
        );

    }

}


export default Sidebar;