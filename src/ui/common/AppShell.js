import React from 'react';
import { Provider } from 'mobx-react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import stores from '../../service/stores';
import { red600, amber500 } from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';


const muiTheme = getMuiTheme({
    appBar: {
        height: 50,
    },
    palette: {
        primary1Color: red600,
    },
    textField: {
        focusColor: amber500,
    }
});


class AppShell extends React.Component {

    render() {

        console.log("[AppShell] - render()");

        return (
            <Provider {...stores}  >
                <MuiThemeProvider muiTheme={muiTheme}>
                    {this.props.children}
                </MuiThemeProvider>
            </Provider>
        );

    }

}


export default AppShell;