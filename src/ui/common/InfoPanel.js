import React from 'react';
import Paper from 'material-ui/Paper';
import './info-panel.scss';
import { observer, inject } from 'mobx-react';

@inject("cardapio")
@observer
class InfoPanel extends React.Component {
    render() {

        const { cardapio } = this.props;
        var empresa = cardapio.empresa || {};



        return (
            <Paper className={`info-panel ${this.props.bigger ? 'bigger' : ''}`} >
                <div className="col col-img">
                    <img src={empresa.urlImagemLogo || "/imgs/restaurant.png"} />
                </div>
                <div className="col col-info" >
                    <h1> {empresa.nomeFantasia || "--"}</h1>
                    <h2> {empresa.telefone || "--"}</h2>
                </div>

            </Paper>
        )

    }
}


export default InfoPanel;