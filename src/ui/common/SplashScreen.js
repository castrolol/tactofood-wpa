import React from 'react';
import { observer, inject } from 'mobx-react';
import './splash-screen.scss';
import { red600 } from 'material-ui/styles/colors';
import CircularProgress from 'material-ui/CircularProgress';

@inject("app", "cardapio")
@observer
class SplashScreen extends React.Component {

    componentWillReact() {

        if (!this.props.app.loaded) {
            if (!this.props.cardapio.loading) {
                setTimeout(() => this.props.app.loaded = true, 1000);
            }
        }

    }


    render() {

        const { app, cardapio } = this.props;

        if (app.loaded) return null;

        var loading = cardapio.loading;

        return (
            <div style={{ background: red600 }} className={`splash-screen ${!loading ? "hide" : ""} `} >
                <div className="content">
                    <h1>Tacto <span> Food </span></h1>
                    {loading ? <CircularProgress size={60} color={"white"} /> : null}
                </div>
            </div>
        );

    }

}


export default (SplashScreen);