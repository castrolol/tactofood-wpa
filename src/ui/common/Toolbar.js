import React from 'react';
import AppBar from 'material-ui/AppBar';
import { APP_NAME } from '../../infra/configs';
import { observer, inject } from 'mobx-react';
import { browserHistory } from 'react-router';

@inject("menu", "app", "nav")
@observer
class Toolbar extends React.Component {


    handleLeftButton() {
        const { nav, menu } = this.props;

        if (nav.canBack) {
            nav.back();
        } else {
            menu.toggle();
        }

    }

    render() {

        const { menu, app, nav } = this.props;

        var leftIconClass = nav.canBack ? "mdi mdi-arrow-left" : "mdi mdi-menu";

        var rightIconClass = undefined;
        var rightAction = undefined;

        if (app.action) {
            rightIconClass = app.actionIcon;
            rightAction = app.action;
        }

        var title = app.title;
        if (typeof title == "function") title = app.title(app);

        return (
            <AppBar
                className="app-toolbar"
                title={title || APP_NAME}
                iconClassNameLeft={leftIconClass}
                onLeftIconButtonTouchTap={() => this.handleLeftButton()}
                iconClassNameRight={rightIconClass}
                onRightIconButtonTouchTap={rightAction}
            />
        );

    }

}


export default (Toolbar);