import React from 'react';
import Paper from 'material-ui/Paper';
import './tacto-food-panel.scss';
import { red600 } from 'material-ui/styles/colors';


class TactoFoodPanel extends React.Component {
    render() {



        return (
            <Paper style={{ background: red600 }} className={`tacto-food-panel`} >

                <h1> Tacto <span>Food</span> </h1>

            </Paper>
        )

    }
}


export default TactoFoodPanel;