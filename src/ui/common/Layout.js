import React from 'react';
import Sidebar from './Sidebar';
import Toolbar from './Toolbar';
import InfoPanel from './InfoPanel';
import './layout.scss';


class Layout extends React.Component {

    render() {

        return <div>
            <Toolbar />
            <div className="app-container">
                <Sidebar />
                {this.props.children}
            </div>
        </div>;

    }

}


export default Layout;