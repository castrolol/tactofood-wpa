import { BASE_URL } from './configs'



export default class Http {

    request(url, method, options = {}) {

        var opts = { ...options, method };
        
        return fetch(`${BASE_URL}${url}`, opts)

    }


    getJson(url) {

        return this.request(url, "GET")
            .then(response => response.json());

    }

    postJson(url, body) {

        return this.request(url, "POST", { body: JSON.stringify(body), headers: new Headers({ "Content-Type": "application/json" }) })
            .then(response => response.json());

    }



}