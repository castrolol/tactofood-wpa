import { browserHistory } from 'react-router';
import { useBasename } from 'history';



const history = useBasename(() => browserHistory)({ basename: "/tactofood/" });
//const history = browserHistory;

export default history;