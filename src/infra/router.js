import React from 'react';
import { Router, IndexRoute, Route, browserHistory } from 'react-router';

import stores from '../service/stores';
import history from './history';

import App from '../ui/app';
import CategoriasPage from '../ui/app/categorias/CategoriasPage';
import GrupoPage from '../ui/app/grupos/GrupoPage';
import AvaliacaoPage from '../ui/app/avaliacao/AvaliacaoPage';
import FeedbackPage from '../ui/app/feedback/FeedbackPage';
 
export default (
    <Router history={history}>

        <Route path="/:empresaId" component={App} onEnter={stores.nav.handleRouteEnter} onChange={stores.nav.handleRouteChange} >
            <IndexRoute component={CategoriasPage} />
            <Route path="grupo/:grupoId" component={GrupoPage} />
            <Route path="avaliacao" component={AvaliacaoPage} />
            <Route path="feedback" component={FeedbackPage} />
        </Route>

    </Router>
)