import Http from '../../infra/http';


export default class FeedbackApi {

    constructor(http) {

        this.http = http || new Http();
    }


    save(empresaId, manifestacoes) {

        return this.http.postJson(`/empresas/${empresaId}/manifestacoes`, manifestacoes);

    }


}