import Http from'../../infra/http';


export default class CardapioApi {

    constructor(http){
        
        this.http = http || new Http();
    }


    get(empresaId){

        return this.http.getJson(`/empresas/${empresaId}/cardapio/completo`);

    }


}