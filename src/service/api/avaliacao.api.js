import Http from'../../infra/http';


export default class AvaliacaoApi {

    constructor(http){
        
        this.http = http || new Http();
    }


    save(empresaId, avaliacao){

        return this.http.postJson(`/empresas/${empresaId}/avaliacoes`, avaliacao);

    }


}