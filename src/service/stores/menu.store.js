import { observable, action } from 'mobx';
 
 
class MenuStore {

    @observable open = false;

    @action toggle = () => {
        this.open = !this.open;
    } 

}


export default new MenuStore();