import { observable, computed, action } from 'mobx';
import history from '../../infra/history';
import cardapioStore from './cardapio.store';

class NavStore {

    @observable backRoute = null;

    @computed get canBack() {
        return this.backRoute != null;
    }

    @action back() {

        var path = this.backRoute;

        history.replace(path);
    }

    handleRouteChange = (prev, next) => {
        this.handleRouteEnter(next);
    }

    handleRouteEnter = (route) => {
        var routes = route.routes.filter(x => x.path && x.component);

        if (routes.length <= 1) {
            this.backRoute = null;
            return;
        }

        var backRoute = routes[routes.length - 2];

        var backPath = backRoute.path.replace(/:([^ \/]*)/gi, function (m, g) {
            return route.params[g];
        });

        this.backRoute = backPath;
    }

}


export default new NavStore();