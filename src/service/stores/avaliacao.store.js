import cardapioStore from './cardapio.store';
import AvaliacaoApi from '../api/avaliacao.api';
import { observable, action } from 'mobx';


class AvaliacaoStore {

    constructor(api) {
        this.api = api || new AvaliacaoApi();
    }

    @observable estrelas = 5;
    @observable comentario = "";
    @observable nome = "";
    @observable email = "";
    @observable telefone = "";

    @observable comentarioError = "";
    @observable nomeError = "";
    @observable emailError = "";
    @observable telefoneError = "";

    @observable sucesso = "";
    @observable error = "";
    @observable loading = false;

    @action enviar = () => {


        this.comentarioError = "";
        this.nomeError = "";
        this.emailError = "";
        this.telefoneError = "";

        if (!this.comentario) this.comentarioError = "Precisamento do seu comentário";
        if (!this.nome) this.nomeError = "Precisamento do seu nome";
        if (!this.email) this.emailError = "Precisamento do seu e-mail";
        if (!this.telefone) this.telefoneError = "Precisamento do seu telefone";


        if (this.comentarioError || this.nomeError || this.emailError || this.telefoneError) {
            this.error = "Preencha as informações para enviar sua avaliação!";
            return;
        }
        this.loading = true;

        this.api.save(cardapioStore.empresaId, {
            nota: this.estrelas,
            avaliador: {

                nome: this.nome,
                email: this.email,
                telefone: this.telefone,
            },
            observacao: this.comentario,

        }).then(res => {
            this.loading = false;
            this.sucesso = "Sua avaliação foi enviada com sucesso!"
            console.log(res);
        }).catch(err => {

            this.loading = false;

            this.error =   "Ocorreu um erro ao enviar sua avaliação.";

        });

    }

    @action limpar = () => {

        this.estrelas = 5;
        this.comentario = "";
        this.nome = "";
        this.email = "";
        this.telefone = "";

        this.comentarioError = "";
        this.nomeError = "";
        this.emailError = "";
        this.telefoneError = "";

        this.error = "";
        this.sucesso = "";

    }

}


export default new AvaliacaoStore();