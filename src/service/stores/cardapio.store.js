import { observable, action } from 'mobx';
import CardapioApi from '../api/cardapio.api';


export class CardapioStore {

    constructor(cardapioApi) {
        this.cardapioApi = cardapioApi || new CardapioApi();
    }

    @observable loading = false;
    @observable empresaId = 0;
    @observable empresaSlug = null;
    @observable error = null;

    @observable empresa = null;
    @observable categorias = [];
    @observable grupos = [];
    @observable produtos = [];
    @observable promocoes = [];



    @action load = (empresaSlug) => {

        if (this.loading) return;
        if (this.empresaSlug == empresaSlug) return;
        this.empresaSlug = empresaSlug;

        this.loading = true;

        this.cardapioApi
            .get(this.empresaSlug)
            .then(({ empresa, categorias, grupos, produtos, promocoes }) => {
                this.loading = false;


                //TODO: da pra ser resolvido apenas com 1 laço em cada... 
                grupos.forEach(grupo => {

                    grupo.produtos = produtos.filter(produto => produto.grupoId == grupo.id);
                    if (!grupo.produtos.length) {
                        grupo.filhos = grupos.filter(grupoFilho => grupoFilho.grupoPaiId == grupo.id);
                        grupo.produtos = null;
                    }

                });

                categorias.forEach(categoria => {
                    categoria.grupos = grupos.filter(x => x.categoriaId == categoria.id);
                });

                this.empresa = empresa;
                this.empresaId = empresa.id;
                this.categorias = categorias;
                this.grupos = grupos;
                this.produtos = produtos;

                this.promocoes = promocoes.sort((a, b) => a.ordem - b.ordem).map(promo => ({
                    image: promo.urlImagemMobile || promo.urlImagemOriginal,
                    text: promo.texto
                }));



            })
            .catch(error => {
                this.loading = false;
                this.error = "Ops... Não conseguimos recuperar o cardápio, tente novamente!";
            });


    }

}


export default new CardapioStore();