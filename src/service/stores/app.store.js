import { observable, action } from 'mobx';



class AppStore {

    @observable title = "Cardápio";
    @observable actionIcon = "mdi mdi-check";
    @observable action = null;

    @action setPage = (title, actionIcon, action) => {

        this.title = title || "";
        this.actionIcon = actionIcon || "mdi mdi-check";
        this.action = action || null;

    }

}


export default new AppStore();