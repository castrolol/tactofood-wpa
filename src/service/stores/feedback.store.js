import FeedbackApi from '../api/feedback.api'
import cardapioStore from './cardapio.store'
import { observable, action } from 'mobx';


class AvaliacaoStore {


    constructor(feedbackApi){
        this.api = feedbackApi || new FeedbackApi();
    }

    @observable tipo = "sugestao";
    @observable descricao = "";
    @observable nome = "";
    @observable email = "";
    @observable telefone = "";

    @observable descricaoError = "";
    @observable nomeError = "";
    @observable emailError = "";
    @observable telefoneError = "";

    @observable error = "";
    @observable sucesso = "";
    @observable loading = false;

    @action enviar = () => {


        this.descricaoError = "";
        this.nomeError = "";
        this.emailError = "";
        this.telefoneError = "";

        if(!this.descricao) this.descricaoError = "Precisamento do seu comentário";
        if(!this.nome) this.nomeError = "Precisamento do seu nome";
        if(!this.email) this.emailError = "Precisamento do seu e-mail";
        if(!this.telefone) this.telefoneError = "Precisamento do seu telefone";


        if (this.descricaoError || this.nomeError || this.emailError || this.telefoneError) {
            this.error = "Preencha as informações para enviar sua avaliação!";
            return;
        }

        this.loading = true;

        this.api.save(cardapioStore.empresaId, {
            tipo: this.tipo == "sugestao" ? 2 : 3,
            manifestante: {

                nome: this.nome,
                email: this.email,
                telefone: this.telefone,
            },
            conteudo: this.descricao,

        }).then(res => {
            this.loading = false;
            this.sucesso = "Seu feedback foi enviado com sucesso!"
            console.log(res);
        }).catch(err => {

            this.loading = false;

            this.error =   "Ocorreu um erro ao enviar seu feedback.";

        });

    }

    @action limpar = () => {

        this.tipo = 5;
        this.descricao = "";
        this.nome = "";
        this.email = "";
        this.telefone = "";

        this.descricaoError = "";
        this.nomeError = "";
        this.emailError = "";
        this.telefoneError = "";


        this.sucesso = "";
        this.error = "";
        this.loading = false;

    }

}


export default new AvaliacaoStore();