import menu from './menu.store';
import app from './app.store';
import cardapio from './cardapio.store';
import nav from './nav.store';
import avaliacao from './avaliacao.store';
import feedback from './feedback.store';

export default {
    menu,
    nav,
    cardapio,
    app,
    avaliacao,
    feedback,
}