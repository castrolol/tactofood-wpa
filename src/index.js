import React from 'react';
import ReactDOM from 'react-dom';
import './infra/theme/theme.scss';
import App from './ui/app';
import AppShell from './ui/common/AppShell';
import injectTouchTap from 'react-tap-event-plugin';
import routes from './infra/router';

injectTouchTap();

console.log("App bootstrap")

ReactDOM.render(
    <AppShell>
        {routes}
    </AppShell>
    , document.getElementById('root'));
